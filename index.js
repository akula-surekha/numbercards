const cards = [
    { class: "one", value: 1 },
    { class: "two", value: 2 },
    { class: "three", value: 3 },
    { class: "four", value: 4 },
    { class: "five", value: 5 },
    { class: "six", value: 6 },
    { class: "seven", value: 7 },
    { class: "eight", value: 8 },
    { class: "nine", value: 9 }
]

function shuffleCards() {
    for (let index = cards.length - 1; index > 0; index--) {
        let randomIndex = randomNumber(index);
        [cards[index], cards[randomIndex]] = [cards[randomIndex], cards[index]];
    }
    document.querySelector('#cardsContainer').innerHTML = updateCards(cards);
}

function sortCards() {
    cards.sort((previous, next) => previous.value - next.value);
    document.querySelector('#cardsContainer').innerHTML = updateCards(cards);
}

function updateCards(cards) {
    let updatedCardsArray = cards.reduce((totalElements, element) => {
        return totalElements + '<span class="card ' + element.class + '"><span class="colorBar ' + element.class + '"></span><span class="number">' + element.value + '</span></span>';
    }, '');
    return updatedCardsArray;
}

function randomNumber(number) {
    return Math.floor(Math.random() * (number + 1));
}